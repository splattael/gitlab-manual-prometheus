# Manual Prometheus for GitLab

Environment to test a self-hosted Prometheus instance (including Alertmanager) with GitLab features (e.g incident management).

It triggers 2 alerts (every 5 seconds) and posts the payload to GitLab's notification endpoint.

## Config

In `alertmanager.yml`, provide URL (`url`) and Authorized key (`bearer_token`) as described in [GitLab's documentation](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#external-prometheus-instances).


## Run

    docker-compose up
